import React from 'react';
import Square from './Square'

class Board extends React.Component {
  renderSquare(i) {
    return (
      <Square
        key={i}
        value={this.props.squares[i]}
        winer={this.props.winners[i]}
        hilight={this.props.hilights[i]}
        onClick={() => this.props.onClick(i)}
      />
    );
  }

  render() {
    let squares = []
    for (let row = 0; row < 3; row++) {
      const child = []
      for (let col = 0; col < 3; col++) {
        const Square = this.renderSquare(row * 3 + col)
        child.push(Square)
      }
      squares.push(<div className="board-row" key={row}>{child}</div>)
    }
    return (
      <div>
        {squares}
      </div>
    );
  }
}

export default Board