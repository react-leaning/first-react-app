import React from 'react';
import Board from './Board'

class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [{
        squares: Array(9).fill(null),
        location: { row: null, col: null },
      }],
      winners: Array(9).fill(false),
      hilights: Array(9).fill(false),
      stepNumber: 0,
      xIsNext: true,
      ifReverse: false,
    };
    this.calculateWinner = this.calculateWinner.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.jumpTo = this.jumpTo.bind(this);
    this.reverseMoves = this.reverseMoves.bind(this);
    this.hoverOn = this.hoverOn.bind(this);
    this.hoverOff = this.hoverOff.bind(this);
  }

  hoverOn(stepNumber) {
    const hilights = this.state.hilights;
    const location = this.state.history[stepNumber].location;
    hilights[(location.row-1) * 3 + location.col-1] = true;
    this.setState({
      hilights: hilights,
    });
  }

  hoverOff(stepNumber) {
    const hilights = this.state.hilights;
    const location = this.state.history[stepNumber].location;
    hilights[(location.row-1) * 3 + location.col-1] = false;
    this.setState({
      hilights: hilights,
    });
  }

  calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
        return lines[i];
      }
    }
    return null;
  }

  jumpTo(step) {
    const history = this.state.history;
    const current = history[step];
    const winner = this.calculateWinner(current.squares);
    const winners = Array(9).fill(false);

    if (winner) {
      winner.forEach(element => {
        winners[element] = true
      });
    }

    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
      winners: winners,
    });
  }

  reverseMoves() {
    this.setState({
      ifReverse: !this.state.ifReverse,
    });
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (this.calculateWinner(squares) || squares[i]) {
      return
    }

    const location = [Math.floor(i / 3) + 1, 1 + i % 3];
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    const winner = this.calculateWinner(squares);
    const winners = this.state.winners.slice();
    if (winner) {
      winner.forEach(element => {
        winners[element] = true
      });
      this.setState({
        winners: winners,
      });
    }
    this.setState({
      history: history.concat([{
        squares: squares,
        location: {
          row: location[0],
          col: location[1],
        },
      }]),
      xIsNext: !this.state.xIsNext,
      stepNumber: history.length,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const winner = this.calculateWinner(current.squares);
    const moves = history.map((step, move) => {
      const desc = move ?
        'Go to move #' + move + ' at row ' + step.location.row + ', col ' + step.location.col :
        'Go to game start';
      return (
        <li key={move} onMouseEnter={() => this.hoverOn(move)} onMouseLeave={() => this.hoverOff(move)}>
          <button onClick={() => this.jumpTo(move)} style={{ fontWeight: this.state.stepNumber === move ? 'bold' : 'normal' }}>{desc}</button>
        </li>
      );
    });

    if (this.state.ifReverse) moves.reverse()

    let status;
    if (winner) {
      status = 'Winner is ' + current.squares[winner[0]];
    } else if (this.state.stepNumber < 9) {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    } else {
      status = 'It\'s a draw!';
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board
            squares={current.squares}
            winners={this.state.winners}
            hilights={this.state.hilights}
            onClick={this.handleClick}
          />
        </div>
        <div className="game-info">
          <div>{status}</div>
          <button onClick={this.reverseMoves}>Reverse moves</button>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

export default Game