import React from 'react';

const Square = (props) => {
  return (
    <button className="square" onClick={props.onClick} style={{ background: props.winer ? 'gray' : 'transparent', border: props.hilight ? '3px solid red' : '1px solid #999' }}>
      {props.value}
    </button>
  );
}

export default Square